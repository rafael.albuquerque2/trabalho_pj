﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour{


    public int Score;

    public Text ScoreTxt;

    public GameObject GOPanel;
      void Start()
    {
       GOPanel.SetActive(false);
       Score = 0;
       ScoreTxt.text = "Score : "+Score;
    }

    public void UpdateScore()
    {
       Score +=100;
       ScoreTxt.text = "Score : "+Score;
    }
    public void GameOver()
    {
       GOPanel.SetActive(true);
    }

        void Update(){

    }
}
